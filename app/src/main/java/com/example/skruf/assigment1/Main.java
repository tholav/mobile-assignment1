package com.example.skruf.assigment1;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Main activity that kicks off the application
 *
 * Populates a list of stored locations
 */
public class Main extends ListActivity {
	String locationDate, locationArea, locationCountry, locationLatitude, locationLongitude, locationTemperature, locationCondition;

	/**
	 * Sets up the list view
	 *
	 * Get stored locations from our database, if any locations were returned, loop through them, put them in a new list and add new HashMaps to it from getLocationDetails
	 * The reason we are doing this is so we can format the TextViews better, instead of just printing data values
	 *
	 * @param savedInstanceState Saved instance state
	 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

		DB db = new DB(this);
		ArrayList<HashMap<String, String>> locations = db.getAll();
		ArrayList<HashMap<String, String>> locationsList = new ArrayList<HashMap<String, String>>();

		if(locations.size() != 0) {
			for(HashMap<String, String> map : locations) {
				locationDate = map.get("locationDate");
				locationArea = map.get("locationArea");
				locationCountry = map.get("locationCountry");
				locationLatitude = map.get("locationLatitude");
				locationLongitude = map.get("locationLongitude");
				locationTemperature = map.get("locationTemperature");
				locationCondition = map.get("locationCondition");

				locationsList.add(getLocationDetails());
			}
		}

		setListAdapter(getListAdapter(locationsList));
    }

	/**
	 * Properly format strings for TextViews
	 *
	 * @return A list of location details
	 */
	public HashMap<String, String> getLocationDetails() {
		HashMap<String, String> locationList = new HashMap<String, String>();
		locationList.put("recorded", "Recorded on " + locationDate + " at " + locationCountry + ", " + locationArea);
		locationList.put("coordinates", "Coordiants for this location are " + locationLatitude + " lat and " + locationLongitude + " lon");
		locationList.put("forecast", "The temperature was " + locationTemperature + "°c and condition was " + locationCondition);

		return locationList;
	}

	/**
	 * Get a new SimpleAdapter
	 *
	 * Define a new SimpleAdapter for our ListView, add data to it and tie values to TextViews
	 *
	 * @param locationsList List of locations
	 * @return A SimpleAdapter
	 */
	public ListAdapter getListAdapter(ArrayList<HashMap<String, String>> locationsList) {
		return new SimpleAdapter(
				Main.this, locationsList, R.layout.location_stored_details,
				new String[] {"recorded", "coordinates", "forecast"},
				new int[] {R.id.recordedTextView, R.id.coordinatesTextView, R.id.forecastTextView}
		);
	}

	/**
	 * Start getLocationDetails activity
	 *
	 * @param view The view
	 */
	public void getLocationCurrentDetails(View view) {
		Intent intent = new Intent(getApplication(), LocationCurrentDetails.class);
		startActivity(intent);
	}
}
