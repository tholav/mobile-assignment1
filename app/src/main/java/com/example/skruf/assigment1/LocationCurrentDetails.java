package com.example.skruf.assigment1;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Activity that gets and sets details about current location
 */
public class LocationCurrentDetails extends Activity {
	TextView recordedTextView, coordinatesTextview, forecastTextView;
	private String date, area, country, latitude, longitude, temperature, condition;
	LocationManager locationManager;
	public LatLng currentLocation;
	public Double currentLatitude, currentLongitude;

	/**
	 * Get current date and time (system time) and format it
	 */
	public LocationCurrentDetails() {
		DateFormat df = new SimpleDateFormat("HH:mm dd/MM/yy");
		Date time = Calendar.getInstance().getTime();
		date = df.format(time);
	}

	/**
	 * Sets location and details about location
	 *
	 * If area was not set show a toast error message to the user
	 *
	 * @param savedInstanceState Saved instance state
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.location_current_details);

		setPosition(lastPosition());
		if(area != null) {
			new setLocationDetails().execute();
		} else {
			Toast.makeText(this, "Unable to find your area!", Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Get last position
	 *
	 * Starts up the location manager and attempts to get last known location stored on the device
	 */
	private Location lastPosition() {
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	}

	/**
	 * Listen for location
	 *
	 * Get a listener that listens to location changes and resets position when it changes
	 */
	private final LocationListener locationListener = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
			setPosition(location);
		}
		public void onStatusChanged(String s, int i, Bundle bundle) {}
		public void onProviderEnabled(String s) {}
		public void onProviderDisabled(String s) {}
	};

	/**
	 * Sets the current location
	 *
	 * Gets the location and updates it using GPS when location has changed. Uses 10 meters and 1 minute between each update
	 * If location was retrieved it gets the longitude and latitude values
	 * If location was not retrieved, show a toast error message to the user
	 *
	 * Inspiration pulled from http://www.androidhive.info/2012/07/android-gps-location-manager-tutorial/
	 */
	public void setPosition(Location location) {
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 60, 10, locationListener);

		if(location != null) {
			currentLatitude = location.getLatitude();
			currentLongitude = location.getLongitude();
			currentLocation = new LatLng(currentLatitude, currentLongitude);
			getLocationDetails(currentLatitude, currentLongitude);
		} else {
			Toast.makeText(this, "Unable to retrieve location! \n Please check your settings", Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Get additional data from location
	 *
	 * Uses Geocoder to define a list of addresses that will contain data for the location,
	 * then sets values from this list by retrieving the data we need
	 *
	 * @param lat Latitude coordinate
	 * @param lon Longitude coordinate
	 */
	public void getLocationDetails(Double lat, Double lon) {
		Geocoder geocoder = new Geocoder(this, Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
			Address location = addresses.get(0);
			country = location.getCountryName();
			area = location.getLocality();
			latitude = String.valueOf(lat);
			longitude = String.valueOf(lon);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save location data to database
	 *
	 * Creates a new HashMap that is filled with key value pairs from data we retrieved earlier
	 * Then calls the insert function, and finally takes us back to main activity
	 *
	 * @param view The view
	 */
	public void storeLocation(View view) {
		DB db = new DB(this);

		HashMap<String, String> location = new HashMap<String, String>();
		location.put("locationDate", date);
		location.put("locationArea", area);
		location.put("locationCountry", country);
		location.put("locationLatitude", latitude);
		location.put("locationLongitude", longitude);
		location.put("locationTemperature", temperature);
		location.put("locationCondition", condition);

		db.insert(location);
		this.gotoMain();
	}

	/**
	 * Get weather data and set TextViews
	 */
	private class setLocationDetails extends AsyncTask<Void, Void, String> {
		private String url1 = "https://query.yahooapis.com/v1/public/yql?q=SELECT%20*%20FROM%20weather.bylocation%20WHERE%20location%3D'";
		private String url2 = "'%20AND%20unit%3D%22c%22&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";
		private String result;
		JSONObject json;

		/**
		 * Get weather data
		 *
		 * This function runs in the background and sends a get request to Yahoo's API service and asks for it to return a JSON object that contains information about the weather
		 * After we get the JSON object we then set strings for temperature and condition by extracting string values from said JSON object
		 *
		 * @return Weather data
		 */
		@Override
		protected String doInBackground(Void... parameters) {
			DefaultHttpClient client = new DefaultHttpClient(new BasicHttpParams());
			HttpPost post = new HttpPost(url1 + area.replaceAll("\\s", "%20") + url2); // Use replaceAll (regex) to make the area string URL-friendly by URL-encoding whitespaces
			post.setHeader("Content-type", "application/json");
			InputStream input;

			try {
				HttpResponse response = client.execute(post);
				HttpEntity entity = response.getEntity();
				input = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(input, "UTF-8"), 8);
				StringBuilder string = new StringBuilder();
				String line;

				while ((line = reader.readLine()) != null) {
					string.append(line).append("\n");
				}

				result = string.toString();
				input.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				json = new JSONObject(result);
				JSONObject weather = json.getJSONObject("query").getJSONObject("results").getJSONObject("weather").getJSONObject("rss").getJSONObject("channel").getJSONObject("item").getJSONObject("condition");
				temperature = weather.getString("temp");
				condition = weather.getString("text");
			} catch(Exception e) {
				e.printStackTrace();
			}

			return result;
		}

		/**
		 * Executes after doInBackground has finished and sets data that has been retrieved to our TextViews
		 * The reason why we set all the TextViews here is because it will then update the view when all of the data has been retrieved
		 *
		 * @param result Result from doInBackground
		 */
		@Override
		protected void onPostExecute(String result) {
			recordedTextView = (TextView) findViewById(R.id.recordedTextView);
			recordedTextView.setText("It's " + date + " and you are in " + country + ", " + area);

			coordinatesTextview = (TextView) findViewById(R.id.coordinatesTextView);
			coordinatesTextview.setText("Your coordinates are: " + latitude + " lat and " + longitude + " lon");

			forecastTextView = (TextView) findViewById(R.id.forecastTextView);
			forecastTextView.setText("The temperature is " + temperature + "°c and conditions are " + condition);
		}
	}

	/**
	 * Start activity to open map view
	 *
	 * Because we are getting the position in this class, we can just send it over to add our marker
	 * so there is no need to find the position again
	 *
	 * Figuring out how to send the LatLng object taken from:
	 * http://stackoverflow.com/questions/16134682/how-to-send-a-latlng-instance-to-new-intent
	 *
	 * @param view The view
	 */
	public void openMap(View view) {
		Bundle location = new Bundle();
		location.putParcelable("current", currentLocation);

		Intent intent = new Intent(getApplication(), LocationCurrentMap.class);
		intent.putExtra("location", location);

		startActivity(intent);
	}

	/**
	 * Go back to main activity
	 *
	 */
	public void gotoMain() {
		Intent intent = new Intent(getApplication(), Main.class);
		startActivity(intent);
	}
}
