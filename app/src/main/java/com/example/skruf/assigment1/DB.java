package com.example.skruf.assigment1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Database class handles an SQLite database
 */
public class DB extends SQLiteOpenHelper{
	private String table = "locations";
	private String prefix = "location";

	/**
	 * Set up a new database called locations
	 *
	 * @param applicationContext Application environment
	 */
	public DB(Context applicationContext) {
		super(applicationContext, "locations.db", null, 1);
	}

	/**
	 * Create table
	 *
	 * Define a query with all of the columns we need, then execute it
	 *
	 * @param db The database object
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		String query = "CREATE TABLE " + table + "(" +
				prefix + "ID INTEGER PRIMARY KEY, " +
				prefix + "Date DATE, " +
				prefix + "Area TEXT, " +
				prefix + "Country TEXT, " +
				prefix + "Latitude DOUBLE, " +
				prefix + "Longitude DOUBLE, " +
				prefix + "Temperature INT, " +
				prefix + "Condition TEXT)";
		db.execSQL(query);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int i, int i2) {
		String query = "DROP TABLE IF EXISTS " + table;
		db.execSQL(query);
		onCreate(db);
	}

	/**
	 * Insert data
	 *
	 * Puts values into table fields by using ContentValues
	 *
	 * @param data Data values
	 */
	public void insert(HashMap<String, String> data) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(prefix + "Date", data.get(prefix + "Date"));
		values.put(prefix + "Area", data.get(prefix + "Area"));
		values.put(prefix + "Country", data.get(prefix + "Country"));
		values.put(prefix + "Latitude", data.get(prefix + "Latitude"));
		values.put(prefix + "Longitude", data.get(prefix + "Longitude"));
		values.put(prefix + "Temperature", data.get(prefix + "Temperature"));
		values.put(prefix + "Condition", data.get(prefix + "Condition"));

		db.insert(table, null, values);
		db.close();
	}

	/**
	 * Get every record in the table
	 *
	 * Puts all records found in an ArrayList with HashMap key value pairs
	 *
	 * @return An ArrayList of locations
	 */
	public ArrayList<HashMap<String, String>> getAll() {
		SQLiteDatabase db = this.getWritableDatabase();
		String query = "SELECT * FROM " + table + " ORDER BY " + prefix + "ID";

		ArrayList<HashMap<String, String>> locations = new ArrayList<HashMap<String, String>>();
		Cursor cursor = db.rawQuery(query, null);

		if(cursor.moveToFirst()) {
			do {
				HashMap<String, String> location = new HashMap<String, String>();
				location.put(prefix + "Date", cursor.getString(1));
				location.put(prefix + "Area", cursor.getString(2));
				location.put(prefix + "Country", cursor.getString(3));
				location.put(prefix + "Latitude", cursor.getString(4));
				location.put(prefix + "Longitude", cursor.getString(5));
				location.put(prefix + "Temperature", cursor.getString(6));
				location.put(prefix + "Condition", cursor.getString(7));
				locations.add(location);
			} while(cursor.moveToNext());
		}
		return locations;
	}
}
