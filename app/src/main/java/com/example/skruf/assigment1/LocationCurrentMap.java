package com.example.skruf.assigment1;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Activity that opens up a map that shows current location
 */
public class LocationCurrentMap extends Activity{
	private GoogleMap map;
	public LatLng position;

	/**
	 * Add a map fragment and set the map up
	 *
	 * @param savedInstanceState Saved instance state
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.location_current_map);

		MapFragment mapFragment = MapFragment.newInstance();
		FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
		fragmentTransaction.add(R.id.map, mapFragment);
		fragmentTransaction.commit();

		getMap();
	}

	/**
	 * Get the map
	 *
	 * Gets current location from previous activity, moves the camera and adds a marker for current location
	 */
	private void getMap() {
		if(map == null) {
			map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
			if(map != null) {
				Bundle bundle = getIntent().getParcelableExtra("location");
				position = bundle.getParcelable("current");
				if(position != null) {
					moveCamera(map);
					map.addMarker(new MarkerOptions().position(position).title("Current Location")).showInfoWindow();
				}
			}
		}
	}

	/**
	 * Move camera map view to position and zoom
	 *
	 * Followed by this example https://developers.google.com/maps/documentation/android/views#target_location
	 *
	 * @param map The map
	 */
	private void moveCamera(GoogleMap map) {
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 2));
		map.animateCamera(CameraUpdateFactory.zoomIn());
		map.animateCamera(CameraUpdateFactory.zoomTo(5), 2000, null);
	}
}
