package com.example.skruf.assigment1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class LocationAdapter extends ArrayAdapter<String> {
	public LocationAdapter(Context context, String[] values) {
		super(context, R.layout.location_stored_details, values);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(getContext());
		View view = inflater.inflate(R.layout.location_stored_details, parent, false);

		String location = getItem(position);
		// TextView textView = (TextView) view.findViewById(R.id.textView);
		// textView.setText(location);

		return view;
	}
}
